import { Given, When, Then, And } from "@badeball/cypress-cucumber-preprocessor"

Given('User is at the login page', () => {
    cy.visit('https://opensource-demo.orangehrmlive.com/')
})

When('User enters username as {string} and password as {string}', (username, password) => {
    cy.get('[name$="username"]').type(username)
    cy.get('[name$="password"]').type(password)
})

Then('User is able to successfully login to the Website', () => {
    cy.get('[type$="submit"]').click()
    cy.title().should('eq', 'Cypress-Automation',{timeout: 10000})
    
})